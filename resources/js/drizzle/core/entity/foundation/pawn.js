import Actor from './actor';

class Pawn extends Actor {
    constructor(object) {
        super(object);

        this.isTick = true;
    }

    tick(delta) { }
}

export default Pawn;
