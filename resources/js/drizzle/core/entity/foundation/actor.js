import { Object3D } from 'three';

class Actor extends Object3D {
    constructor(object = null) {
        super();

        this.isNative = true;
        this.isTick = false;

        this.add(
            this.spawn(object)
        );
    }

    spawn(object) { }
}

export default Actor;
