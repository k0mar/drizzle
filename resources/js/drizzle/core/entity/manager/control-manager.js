import Manager from '../../tools/model/manager';

class ControlManager extends Manager {
    implements() {
        this.keyboard = {};
    }

    realised(keyCode, isPressed) {
        this.keyboard[keyCode] = isPressed;
    }

    event() {
        this.boundKeyDown = event => this.realised(event.which, true);
        this.boundKeyUp = event => this.realised(event.which, false);

        this.domElement.addEventListener('keydown', this.boundKeyDown, false);
        this.domElement.addEventListener('keyup', this.boundKeyUp, false);
    }
}

export default ControlManager;
