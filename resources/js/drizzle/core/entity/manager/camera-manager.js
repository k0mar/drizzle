import Manager from '../../tools/model/manager';
import { Vector2, Vector3 } from 'three';

class CameraManager extends Manager {
    implements() {
        this.sensitivity = new Vector2(1, 1);
        this.target = new Vector3();
        this.character = {};

        this.radius = 15;
        this.theta = 0;
        this.phi = 0;
    }

    realised(deltaX, deltaY) {
        this.theta -= deltaX * this.sensitivity.x;
        this.theta %= 720;

        this.phi += deltaY * this.sensitivity.y;
        this.phi = Math.min(170, Math.max(-170, this.phi));
    }

    event() {
        this.boundLockedPointer = () => this.domElement.requestPointerLock();
        this.boundMouseMove = event => this.realised(event.movementX, event.movementY);

        this.domElement.addEventListener('mousedown', this.boundLockedPointer, false);
        this.domElement.addEventListener('mousemove', this.boundMouseMove, false);
    }
}

export default CameraManager;
