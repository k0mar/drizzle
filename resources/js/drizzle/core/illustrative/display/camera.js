import { PerspectiveCamera } from 'three';
import config from '../../tools/settings/config';

class Camera extends PerspectiveCamera {
    constructor() {
        const cameraConfig = config.user.camera;
        super(cameraConfig.fov, cameraConfig.aspect, cameraConfig.near, cameraConfig.far);
    }

    boot() {}

    tick() {}
}

export default Camera;
