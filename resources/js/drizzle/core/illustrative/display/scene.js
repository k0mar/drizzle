import { Scene as SceneThree, Color, Fog } from 'three';
import config from '../../tools/settings/config';

class Scene extends SceneThree {
    boot() {
        const graphicsConfig = config.kernel.graphics;
        const cameraConfig = config.user.camera;

        this.background = new Color().setHex(graphicsConfig.background.color);
        this.fog = new Fog(this.background, cameraConfig.near, cameraConfig.far);
    }
}

export default Scene;
