import { WebGLRenderer } from 'three';

class Renderer extends WebGLRenderer {
    boot() {
        this.gammaInput = true;
        this.gammaOutput = true;
        this.shadowMap.enabled = true;

        this.setPixelRatio(window.devicePixelRatio);
        this.setSize(window.innerWidth, window.innerHeight);

        document.body.appendChild(this.domElement);
    }
}

export default Renderer;
