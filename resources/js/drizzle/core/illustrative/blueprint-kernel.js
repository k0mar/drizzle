import { EventDispatcher } from 'three';
import Storage from '../tools/model/Storage';

class BlueprintKernel extends EventDispatcher {
    constructor(resources) {
        super();

        this.storage = new Storage(resources);
        this.models = {};
    }

    add(model) {
        this.models[model.key] = model.value;
        this.dispatchEvent({type: 'addModel', objects: model.value});
    }

    remove(model) {
        this.dispatchEvent({type: 'removeModel', objects: model.value});
    }
}

export default BlueprintKernel;
