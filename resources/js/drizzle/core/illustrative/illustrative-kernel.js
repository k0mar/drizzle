import Scene from './display/scene';
import Renderer from './display/renderer';
import Loader from '../tools/model/loader';

import {Clock, Group, AxesHelper } from 'three';

class IllustrativeKernel {
    constructor() {
        this.clock = new Clock();

        this.scene = new Scene();
        this.renderer = new Renderer();
        this.loader = new Loader();

        this.blueprint = {};
        this.resources = {};
        this.camera = {};
        this.universe = {};
        this.stats = {};

        this.needTick = [];
    }

    boot() {
        this.scene.boot();
        this.renderer.boot();
        this.camera.boot(this.renderer.domElement);
    }

    load() {
        let world = new Group();
        this.scene.add(world);
        this.universe = this.blueprint.universe();

        world.add(new AxesHelper(1000));

        for (let key in this.universe) {
            if (this.universe[key].isTick) {
                this.needTick.push(key);
            }

            world.add(this.universe[key]);
        }
    }

    tick() {
        let delta = this.clock.getDelta();

        for (let key of this.needTick) {
            this.universe[key].tick(delta);
        }

        this.camera.tick(this.universe['mainPlayer']);

        this.renderer.render(this.scene, this.camera);
    }
}

export default IllustrativeKernel;
