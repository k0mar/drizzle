const config = {
    kernel: {
        graphics: {
            background: {
                color: 0xffffff
            }
        }
    },
    user: {
        camera: {
            fov: 45,
            aspect: window.innerWidth / window.innerHeight,
            near: 3,
            far: 3000
        }
    }
};

export default config;
