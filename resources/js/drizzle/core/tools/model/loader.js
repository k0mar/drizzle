import GLTFLoader from 'three-gltf-loader';
import { TextureLoader } from 'three';

class Loader {
    constructor() {
        this.gltf = new GLTFLoader();
        this.texture = new TextureLoader();
    }

    loadTexture(path, name) {
        return new Promise((resolve, reject) => {
            this.texture.load(path, map => {
                map.name = name;
                resolve(map);
            });
        });
    }

    loadModel(path, name) {
        return new Promise((resolve, reject) => {
            this.gltf.load(path, object => {
                object.name = name;
                resolve(object);
            });
        })
    }
}

export default Loader;
