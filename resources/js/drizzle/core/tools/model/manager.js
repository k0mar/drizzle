class Manager {
    constructor(domElement = null) {
        this.domElement = (domElement === null) ? document : domElement;

        this.event();
        this.implements();
    }

    event() { }
    implements() { }
}

export default Manager;
