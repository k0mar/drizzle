import Actor from '../../../../../core/entity/foundation/actor';

class Component extends Actor {
    spawn(object) {
        return object.scene;
    }
}

export default Component;
