import Character from '../../../../../core/entity/foundation/character';
import { Vector3 } from 'three';

class Player extends Character {
    spawn(object) {
        return object.scene;
    }

    tick(delta) {
        let rotation = 30 * delta;
        this.rotateOnAxis(new Vector3(0, 1, 0), rotation);
    }
}

export default Player;
