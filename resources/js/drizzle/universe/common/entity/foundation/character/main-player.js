import ControlManager from '../../../../../core/entity/manager/control-manager';
import Character from '../../../../../core/entity/foundation/character';

class MainPlayer extends Character {
    spawn(object) {
        this.controlManager = new ControlManager();

        this.moveSpeedForwad = 14;
        this.moveSpeedBack = 3;
        this.rotateSpeed = 3;

        return object.scene;
    }

    tick(delta) {
        let moveForward = this.moveSpeedForwad * delta;
        let moveBack = this.moveSpeedBack * delta;
        let rotation = this.rotateSpeed * delta * (Math.PI / 2);

        if (this.controlManager.keyboard[87] === true) {
            this.translateZ(moveForward);
        }

        if (this.controlManager.keyboard[83] === true) {
            this.translateZ(-moveBack);
        }
    }
}

export default MainPlayer;
