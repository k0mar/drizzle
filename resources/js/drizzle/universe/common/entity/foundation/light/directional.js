import {DirectionalLight} from 'three';

class Directional extends DirectionalLight {
    constructor() {
        super(0xffffff, 1.5);

        this.implements();
    }

    implements() {
        this.color.setHSL(0.1, 1, 0.95);
        this.position.set(- 1, 1.75, 1);
        this.position.multiplyScalar(30);
    }
}

export default Directional;
