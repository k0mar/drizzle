import {HemisphereLight} from 'three';

class Hemisphere extends HemisphereLight {
    constructor() {
        super(0xffffff, 0xffffff, 1.5);

        this.implements();
    }

    implements() {
        this.color.setHSL(0.6, 1, 0.6);
        this.groundColor.setHSL(0.095, 1, 0.75);
        this.position.set(0, 80, 0);
    }
}

export default Hemisphere;
