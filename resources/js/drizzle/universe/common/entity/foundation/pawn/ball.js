import Pawn from '../../../../../core/entity/foundation/pawn';
import {Mesh, MeshBasicMaterial, SphereGeometry, Vector3, TextureLoader, BackSide} from 'three';

class Ball extends Pawn {
    spawn(object) {
        return new Mesh(
            new SphereGeometry(2000, 50, 50),
            new MeshBasicMaterial(
                {
                    map: new TextureLoader().load('images/textures/skydome-light.jpg'),
                    side: BackSide
                }
            )
        );
    }

    tick(delta) {
        let rotation = 0.05 * delta * (Math.PI / 2);
        this.rotateOnAxis(new Vector3(0, 1, 0), rotation);
    }
}

export default Ball;
