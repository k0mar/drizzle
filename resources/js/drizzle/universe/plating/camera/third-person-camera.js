import Camera from '../../../core/illustrative/display/camera';
import CameraManager from '../../../core/entity/manager/camera-manager';
import { Vector3, Quaternion} from 'three';

class ThirdPersonCamera extends Camera {
    boot(domElement) {
        this.cameraManager = new CameraManager(domElement);
    }

    tick(target = null) {
        if (target !== null) {
            this.cameraManager.target.applyMatrix4(target.matrixWorld);
            this.cameraManager.target.set(target.position.x, target.position.y + 2, target.position.z);
        }

        let targetX = this.cameraManager.target.x;
        let targetY = this.cameraManager.target.y;
        let targetZ = this.cameraManager.target.z;

        let radius = this.cameraManager.radius;
        let theta = this.cameraManager.theta;
        let phi = this.cameraManager.phi;

        let posX = targetX + radius * Math.sin(theta * Math.PI / 360) * Math.cos(phi * Math.PI / 360);
        let posY = targetY + radius * Math.sin(phi * Math.PI / 360);
        let posZ = targetZ + radius * Math.cos(theta * Math.PI / 360) * Math.cos(phi * Math.PI / 360);


        this.position.set(posX, posY, posZ);
        this.lookAt(this.cameraManager.target);
    }
}

export default ThirdPersonCamera;
