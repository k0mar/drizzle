import IllustrativeKernel from '../../core/illustrative/illustrative-kernel';

class Kernel extends IllustrativeKernel {
    data() {
        return [
            this.loader.loadModel('objects/elaine/elaine.gltf', 'elaine').then(
                object => object
            ),
            this.loader.loadModel('objects/ground/ground.gltf', 'ground').then(
                object => object
            ),
        ]
    }
}

export default Kernel;
