import BlueprintKernel from '../../core/illustrative/blueprint-kernel';

import Ball from '../common/entity/foundation/pawn/ball';

import Hemisphere from '../common/entity/foundation/light/hemisphere';
import Directional from '../common/entity/foundation/light/directional';

import Component from '../common/entity/foundation/actor/component';
import MainPlayer from '../common/entity/foundation/character/main-player';

class Blueprint extends BlueprintKernel {
    universe() {
        return {
            skyDome: new Ball(),
            hemisphereLight: new Hemisphere(),
            directionalLight: new Directional(),
            ground: new Component(this.storage.get('ground')),
            mainPlayer: new MainPlayer(this.storage.get('elaine')),
        }
    }
}

export default Blueprint;
