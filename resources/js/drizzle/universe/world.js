import Kernel from './plating/kernel';
import Blueprint from './plating/blueprint';
import ThirdPersonCamera from './plating/camera/third-person-camera';
import Stats from 'stats-js/src/Stats';

class World {
    constructor() {
        this.kernel = new Kernel();
        this.kernel.camera = new ThirdPersonCamera();
        this.stats = new Stats();

        this.stats.showPanel(0);

        document.body.appendChild(this.stats.dom);
    }

    run() {
        Promise.all(this.kernel.data()).then(
            resources => {
                this.kernel.blueprint = new Blueprint(resources);

                this.kernel.load();
                this.kernel.boot();

                this.tick();
            }
        );
    }

    tick() {

        this.stats.begin();

        this.kernel.tick();

        this.stats.end();
        requestAnimationFrame(() => this.tick());
    }
}

export default World;
